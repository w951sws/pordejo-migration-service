package com.connecture.services.pordejomigrationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PordejoMigrationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PordejoMigrationServiceApplication.class, args);
	}

}
